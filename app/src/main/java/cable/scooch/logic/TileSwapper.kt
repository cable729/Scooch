package cable.scooch.logic

interface TileSwapper {
    fun swap(x1: Int, y1: Int, x2: Int, y2: Int): Boolean
}