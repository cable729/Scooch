package cable.scooch

import cable.scooch.logic.StandardTileSwapper
import org.junit.Test

class TileSwappingTests {
    @Test
    fun successfulSwap() {
        val swapper = StandardTileSwapper()
        val result = swapper.swap(0, 0, 1, 0)

        assert(result)
    }
}